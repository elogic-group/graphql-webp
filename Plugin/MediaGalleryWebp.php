<?php
declare(strict_types=1);

namespace Elogic\PwaWebpConverter\Plugin;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Yireo\NextGenImages\Exception\ConvertorException;
use Yireo\NextGenImages\Image\ImageFactory;
use Yireo\Webp2\Convertor\Convertor;
use Yireo\NextGenImages\Config\Config;
use Elogic\PwaWebpConverter\Util\FilePath;


/**
 * Generate webp image and replaces the path of the original jpg/jpeg/png image with a new one;
 * Plugin on deprecated class;
 */
class MediaGalleryWebp
{
    /**
     * @var Convertor
     */
    private $convertor;
    /**
     * @var ImageFactory
     */
    private $imageFactory;
    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @param Convertor $convertor
     * @param ImageFactory $imageFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(

        Convertor             $convertor,
        ImageFactory          $imageFactory,
        StoreManagerInterface $storeManager,
        Config                $config,
        FilePath              $filePath
    )
    {
        $this->convertor = $convertor;
        $this->imageFactory = $imageFactory;
        $this->_storeManager = $storeManager;
        $this->config = $config;
        $this->filePath = $filePath;
    }

    /**
     * @param ResolverInterface $subject
     * @param $result
     * @return array
     * @throws FileSystemException
     * @throws NoSuchEntityException
     * @throws ConvertorException
     */
    public function afterResolve(ResolverInterface $subject, $result): array
    {
        foreach ($result as $key => $item) {
            $path = $result[$key]['file'];
            $url = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $path;
            $image = $this->imageFactory->createFromUrl($url);
            if($this->filePath->ifFileExist($image->getPath())){
                $webpImage = $this->convertor->convertImage($image);
                $result[$key]['file'] = preg_replace('/\/[^\/]*$/', '/' . $this->getConvertFilename($image, 'webp'), $item['file']);
            }
        }
        return $result;
    }

    /**
     * @param $image
     * @param string $suffix
     * @return string
     */
    private function getConvertFilename($image, string $suffix): string
    {
        $filename = basename($image->getPath());
        $path = preg_replace('/\.(jpg|jpeg|png)$/', '', $filename);
        if(!$this->config->addHash()) {
            return $path . '.' . $suffix;
        } else {return $path . $this->getHash($image) . '.' . $suffix;}
    }

    /**
     * @param $image
     * @return string
     */
    private function getHash($image): string
    {
        return '-' . hash('crc32', $image->getPath());
    }

}
