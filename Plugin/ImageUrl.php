<?php
declare(strict_types=1);

namespace Elogic\PwaWebpConverter\Plugin;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Yireo\NextGenImages\Exception\ConvertorException;
use Yireo\NextGenImages\Image\ImageFactory;
use Yireo\Webp2\Convertor\Convertor;
use Elogic\PwaWebpConverter\Util\FilePath;

/**
 * Generate webp image and replaces the path of the original jpg/jpeg/png image with a new one;
 */
class ImageUrl
{
    /**
     * @var Convertor+
     */
    private $convertor;
    /**
     * @var ImageFactory
     */
    private $imageFactory;

    /**
     * @param Convertor $convertor
     * @param ImageFactory $imageFactory
     */
    public function __construct(

        Convertor    $convertor,
        ImageFactory $imageFactory,
        FilePath $filePath
    )
    {
        $this->convertor = $convertor;
        $this->imageFactory = $imageFactory;
        $this->filePath = $filePath;
    }

    /**
     * @param ResolverInterface $subject
     * @param $imageUrl
     * @return string
     * @throws FileSystemException
     * @throws NoSuchEntityException
     * @throws ConvertorException
     */
    public function afterResolve(ResolverInterface $subject, $imageUrl): string
    {
        $image = $this->imageFactory->createFromUrl($imageUrl);
        if($this->filePath->ifFileExist($image->getPath())) {
            $webpImage = $this->convertor->convertImage($image);
            return $webpImage->getUrl();
        } else {
            return $imageUrl;
        }
    }
}
