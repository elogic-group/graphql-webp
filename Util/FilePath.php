<?php

namespace Elogic\PwaWebpConverter\Util;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\DriverInterface;

/**
 * Check if file exist
 */
class FilePath
{

    /**
     * @param Filesystem $filesystem
     * @throws FileSystemException
     */
    public function __construct
    (
        Filesystem $filesystem
    )
    {
        $this->fileDriver = $filesystem->getDirectoryWrite(DirectoryList::PUB)->getDriver();
    }

    /**
     * @param $imagePath
     * @return bool
     */
    public function ifFileExist($imagePath): bool
    {
        try {
            return $this->fileDriver->isExists($imagePath);
        } catch (FileSystemException $fileSystemException) {
            return false;
        }
    }
}
